create table short_urls (
    id integer not null primary key,
    original_url varchar not null,
    short_url_path varchar default null,
    created_at datetime not null default current_timestamp,
    updated_at datetime not null default current_timestamp
);

create unique index idx_short_urls_short_url_path
on short_urls (short_url_path);
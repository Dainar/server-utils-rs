use std::env;

pub struct AppConfig {
    pub db_url: String
}

impl AppConfig {
    pub fn new() -> AppConfig {
        AppConfig { 
            db_url: env::var("DATABASE_URL").unwrap_or(String::from("sqlite://utils.db")) 
        }
    }
}

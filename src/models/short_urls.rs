use serde::{Deserialize, Serialize};

#[derive(sqlx::FromRow, Deserialize, Serialize)]
pub struct ShortUrl {
    pub id: i32,
    pub original_url: String,
    pub short_url_path: String,
    pub created_at: String,
    pub updated_at: String,
}

#[derive(Deserialize)]
pub struct CreateShortUrl {
    pub original_url: String,
    pub short_url_path: String,
}

use axum::{
    extract::Extension, extract::Json, http::StatusCode, response::IntoResponse, routing::get,
    routing::post, Router,
};
use chrono::{DateTime, Local, Utc};
use sqlx::sqlite::{SqlitePool, SqlitePoolOptions};
mod config;
mod models;

const SERVER_HOST: &str = "0.0.0.0:3000";

#[tokio::main]
async fn main() {
    let app_config = config::AppConfig::new();
    let pool = SqlitePoolOptions::new()
        .max_connections(50)
        .connect(&app_config.db_url)
        .await
        .unwrap();

    // sqlx::migrate!()
    //     .run(&pool)
    //     .await
    //     .unwrap();

    let app = Router::new()
        .route("/short-urls", get(get_short_urls))
        .route("/short-urls", post(create_short_url))
        .layer(Extension(pool));

    println!("Server running on {}", SERVER_HOST);
    axum::Server::bind(&SERVER_HOST.parse().unwrap())
        .serve(app.into_make_service())
        .await
        .unwrap();
}

async fn create_short_url(
    Extension(pool): Extension<SqlitePool>,
    Json(payload): Json<models::short_urls::CreateShortUrl>,
) -> impl IntoResponse {
    // let local_time = Local::now();
    // let utc_time = DateTime::<Utc>::from_naive_utc_and_offset(local_time.naive_utc(), Utc).to_string();
    sqlx::query("INSERT INTO short_urls (original_url, short_url_path) VALUES (?, ?)")
        .bind(payload.original_url)
        .bind(payload.short_url_path)
        .execute(&pool)
        .await
        .unwrap();

    StatusCode::CREATED
}

async fn get_short_urls(Extension(pool): Extension<SqlitePool>) -> impl IntoResponse {
    let short_url = sqlx::query_as::<_, models::short_urls::ShortUrl>("SELECT * FROM short_urls")
        .fetch_all(&pool)
        .await
        .unwrap();

    (StatusCode::OK, Json(short_url))
}

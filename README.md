# Server utils

## Development

1. Install rust
2. For the database install the sqlx cli (`cargo install sqlx-cli`)
3. Create a `.env` file in the root of the folder and add
   ```sh
   DATABASE_URL=sqlite://utils.db
   JWT_SECRET=<token-secret>
   JWT_EXPIRY_DURATION=<any-amount-of-time>
   ```
4. Run `sqlx database create` the first time to create the database

## cURL

### Add short url

```sh
curl -X POST http://localhost:3000/short-urls \
-H "Content-Type: application/json" \
-d '{"original_url": "https://mywebapp.com/my/route", "short_url_path": "mycustompath"}'
```
